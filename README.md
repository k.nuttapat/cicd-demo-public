# CI-CD Demo

> For more in-depth information, consult: https://docs.gitlab.com/ee/ci/README.html

## Description:

1. Lint (allow fail)
    - flake8 
2. Test
    - pytest
    - required database service
3. SonarQube
    - pytest (can be cached from previous step)
    - sonarqube from coverage.xml
4. build Docker image
    - get credentials from secret and login
    - build and tag
    - push 
5. Deploy on GKE
    - get credentials from secret and login
    - authenticate with `gcloud container get-credentails`
    - deploy using `ci/k8s` folder
 
## Simple flow
`ci/gitlab/basic.yml`

Topic cover:
- Basic GitlabCI structure
    - stage and job
    - job structure
    - image
    - variables
    - artifacts
    - script, before_script, after_script
    - [only](https://docs.gitlab.com/ee/ci/yaml/#onlyexcept-basic)
    - allow_failure
    - secrets
    
## Refactor No.1
`ci/gitlab/refactor01.yml`
Main goal: separate jobs into files
(more like advanced yaml)

Topic Cover:
- extends (within 1 file and multiple files)
- include
- anchor
    
## Refactor No.2
`ci/gitlab/refactor02.yml`

Main goal: Less variable = less confusing DevOps

Topic Cover:
- environments
- manage multiple env variables
