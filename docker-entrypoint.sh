#!/bin/sh
set -e

#DATABASE_URL="postgres://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME}"
until psql "$DATABASE_URL" -c '\l'; do
    nslookup postgres
    >&2 echo "Postgres is unavailable - sleeping"
    sleep 1
done

>&2 echo "Postgres is up - continuing"

if [ "$DJANGO_MANAGEPY_MIGRATE" = 'xon' ]; then
    python manage.py migrate --noinput
#    python manage.py loaddata user/fixtures/users.json
fi

exec "$@"