FROM python:3.7
ENV PYTHONUNBUFFERED 1

WORKDIR /code
RUN pip install --upgrade pip
RUN pip install pipenv
RUN apt-get update && apt-get install -y dnsutils postgresql-client && rm -rf /var/lib/apt/lists/*
COPY Pipfile* /code/
RUN pipenv install --system --deploy

ADD . /code/
RUN mkdir -p /code/pipe/uploaded_files/

# sanity work
ARG git_commit
ARG version
RUN echo "git_commit: $git_commit\nversion: $version"

# uWSGI will listen on this port
EXPOSE 8000

# Call collectstatic (customize the following line with the minimal environment variables needed for manage.py to run):
RUN python manage.py collectstatic --noinput

# Tell uWSGI where to find your wsgi file (change this):
ENV UWSGI_WSGI_FILE=HelloApp/wsgi.py

# Base uWSGI configuration (you shouldn't need to change these):
ENV UWSGI_HTTP=:8000 UWSGI_MASTER=1 UWSGI_HTTP_AUTO_CHUNKED=1 UWSGI_HTTP_KEEPALIVE=1 UWSGI_LAZY_APPS=1 UWSGI_WSGI_ENV_BEHAVIOR=holy

# Number of uWSGI workers and threads per worker (customize as needed):
ENV UWSGI_WORKERS=1 UWSGI_THREADS=4

# uWSGI static file serving configuration (customize or comment out if not needed):
ENV UWSGI_STATIC_MAP="/backend/static/=/code/backend/static/" UWSGI_STATIC_EXPIRES_URI="/backend/static/.*\.[a-f0-9]{12,}\.(css|js|png|jpg|jpeg|gif|ico|woff|ttf|otf|svg|scss|map|txt) 315360000"

# Deny invalid hosts before they get to Django (uncomment and change to your hostname(s)):
# ENV UWSGI_ROUTE_HOST="^(?!localhost:8000$) break:400"

# Uncomment after creating your docker-entrypoint.sh
# ENTRYPOINT ["/code/docker-entrypoint.sh"]
ENV PRODUCTION=True
RUN chmod +x /code/docker-entrypoint.sh

# Start uWSGI
ENTRYPOINT ["/code/docker-entrypoint.sh"]

CMD ["uwsgi"]