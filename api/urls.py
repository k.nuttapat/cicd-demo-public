from django.urls import path, include
from rest_framework.routers import DefaultRouter

from . import views
from .views import hello_world_view
router = DefaultRouter(trailing_slash=False)
router.register(r"user", views.UserViewSet, "user")
urlpatterns = [
    path(r'hello/', hello_world_view, name='hello'),
    path(r'', include((router.urls, 'router')), name='router')
]
