from django.http import JsonResponse
# Create your views here.
from rest_framework.viewsets import ModelViewSet

from api.models import User
from api.serializers import UserSerializer


def hello_world_view(request):
    return JsonResponse({"message": "helloworld"}, safe=False)


class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
