# Create your tests here.
import json

from django.http import JsonResponse
from django.urls import reverse
from rest_framework.test import APITestCase

from api.models import User


class HelloTestCase(APITestCase):
    def test_get_hello(self):
        resp: JsonResponse = self.client.get('/api/hello/')
        got = json.loads(resp.content)
        self.assertIn("message", got)
        self.assertEqual(got['message'], "helloworld")
        self.assertEqual(resp.status_code, 200)


class UserTestCases(APITestCase):
    def setUp(self):
        self.user = User.objects.create(
            first_name="John",
            last_name="Doe"
        )

    def test_get_user(self):
        resp: JsonResponse = self.client.get(reverse('api:router:user-detail', args=[self.user.id]))
        self.assertEqual(resp.status_code, 200)
        got = json.loads(resp.content)
        print(got)
        self.assertEqual(got['first_name'], 'John')
